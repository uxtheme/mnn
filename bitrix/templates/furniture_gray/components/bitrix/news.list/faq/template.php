<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="page-content">
	<?foreach($arResult["SECTIONS"] as $arItem):?>
		<h2 class="heading-block"><?=$arItem['NAME']?></h2>
		<?foreach($arResult["ITEMS"] as $item):?>
			<?if($item['IBLOCK_SECTION_ID']==$arItem['ID']):?>
				<div class="accordion-item">
					<div class="accordion-item__header">
						<span class="accordion-item__header-title"><?=$item['NAME']?></span>
					</div>
					<div class="accordion-item__content" style="display:none">
						<p><?=$item['~PREVIEW_TEXT']?></p>
					</div>
				</div>
			<?endif;?>
		<?endforeach;?>	
		<div style="height:50px" aria-hidden="true" class="wp-block-spacer"></div>
	<?endforeach;?>
</div>