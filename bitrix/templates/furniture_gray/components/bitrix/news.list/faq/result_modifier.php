<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
  $arFilter = Array('IBLOCK_ID'=>$IBLOCK_ID, 'GLOBAL_ACTIVE'=>'Y');
  $db_list = CIBlockSection::GetList(Array("SORT"=>"ASC"),Array($by=>$id), $arFilter, true);
  while($ar_result = $db_list->GetNext())
  {
    $arResult['SECTIONS'][$ar_result['ID']]['ID'] = $ar_result['ID'];
    $arResult['SECTIONS'][$ar_result['ID']]['NAME'] = $ar_result['NAME'];
  }
