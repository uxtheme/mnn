<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
IncludeTemplateLangFile(__FILE__);
?>
<!doctype html>
<!--[if lt IE 9]><html class="no-js no-svg ie lt-ie9 lt-ie8 lt-ie7" lang="ru-RU"> <![endif]-->
<!--[if IE 9]><html class="no-js no-svg ie ie9 lt-ie9 lt-ie8" lang="ru-RU"> <![endif]-->
<!--[if gt IE 9]><!--><html class="no-js no-svg" lang="ru-RU"> <!--<![endif]-->
<head>
    <meta charset="UTF-8" />
    <meta name="description" content="минималистичные купальники и одежда">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="style.css" type="text/css" media="screen" />
    <link rel="pingback" href="https://mynudenymph.com/xmlrpc.php" />
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <script>
        const $ = jQuery.noConflict();
    </script>
	<?$APPLICATION->ShowHead();?>
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/page.css" type="text/css" rel="stylesheet">
</head>

<body class="body">
	<?$APPLICATION->ShowPanel();?>
    <header class="header header_left">
        <a href="https://mynudenymph.com" class="header__logo">
            <svg>
                <use xlink:href='#logo' />
            </svg>
        </a>
        <nav class="header__menu">
            <ul class="header__menu__parent">

                <li class=" menu-item menu-item-type-custom menu-item-object-custom menu-item-34 menu-item-has-children ">
                    <a target="" href="#">магазин</a>
                    <ul class="header__menu__children">

                        <li class=" menu-item menu-item-type-post_type menu-item-object-page menu-item-173 ">
                            <a target="" href="https://mynudenymph.com/shop/">смотреть все</a>
                        </li>

                        <li class="menu-item_outlet menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-22221 ">
                            <a target="" href="https://mynudenymph.com/shop/outlet/">распродажа</a>
                        </li>

                        <li class=" menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-1222 ">
                            <a target="" href="https://mynudenymph.com/shop/beachwear/">купальники</a>
                        </li>

                        <li class=" menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-3127 ">
                            <a target="" href="https://mynudenymph.com/shop/apparel/">одежда</a>
                        </li>

                        <li class=" menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-11030 ">
                            <a target="" href="https://mynudenymph.com/shop/activewear/">для спорта</a>
                        </li>

                        <li class=" menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-2998 ">
                            <a target="" href="https://mynudenymph.com/shop/accessories/">аксессуары</a>
                        </li>

                        <li class=" menu-item menu-item-type-post_type menu-item-object-product menu-item-4809 ">
                            <a target="" href="https://mynudenymph.com/product/gift-card/">подарочные карты</a>
                        </li>
                    </ul>
                </li>

                <li class=" menu-item menu-item-type-custom menu-item-object-custom menu-item-3040 menu-item-has-children ">
                    <a target="" href="#">коллекции</a>
                    <ul class="header__menu__children">

                        <li class=" menu-item menu-item-type-post_type menu-item-object-page menu-item-27821 ">
                            <a target="" href="https://mynudenymph.com/collection-summer-22/">Лето ’22</a>
                        </li>

                        <li class=" menu-item menu-item-type-post_type menu-item-object-page menu-item-17944 ">
                            <a target="" href="https://mynudenymph.com/tales/">Tales</a>
                        </li>

                        <li class=" menu-item menu-item-type-post_type menu-item-object-page menu-item-4401 ">
                            <a target="" href="https://mynudenymph.com/lido/">Lido</a>
                        </li>

                        <li class=" menu-item menu-item-type-post_type menu-item-object-page menu-item-3038 ">
                            <a target="" href="https://mynudenymph.com/collection-spring-21/">Весна ’21</a>
                        </li>

                        <li class=" menu-item menu-item-type-post_type menu-item-object-page menu-item-3037 ">
                            <a target="" href="https://mynudenymph.com/collection-winter-21/">New Year ’21</a>
                        </li>

                        <li class=" menu-item menu-item-type-post_type menu-item-object-page menu-item-3036 ">
                            <a target="" href="https://mynudenymph.com/collection-summer-20/">Лето ’20</a>
                        </li>
                    </ul>
                </li>

                <li class=" menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor current-menu-parent menu-item-3041 menu-item-has-children active">
                    <a target="" href="#">помощь</a>
                    <ul class="header__menu__children">

                        <li class=" menu-item menu-item-type-post_type menu-item-object-page menu-item-3039 ">
                            <a target="" href="https://mynudenymph.com/size-table/">таблица размеров</a>
                        </li>

                        <li class=" menu-item menu-item-type-post_type menu-item-object-page menu-item-3061 ">
                            <a target="" href="https://mynudenymph.com/shipping-and-returns/">доставка и возврат</a>
                        </li>

                        <li class=" menu-item menu-item-type-post_type menu-item-object-page menu-item-3059 ">
                            <a target="" href="https://mynudenymph.com/discounts/">акции и скидки</a>
                        </li>

                        <li class=" menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-3053 current_page_item menu-item-3060 active">
                            <a target="" href="https://mynudenymph.com/faq/">FAQ</a>
                        </li>
                    </ul>
                </li>

                <li class=" menu-item menu-item-type-post_type menu-item-object-page menu-item-27962 ">
                    <a target="" href="https://mynudenymph.com/about-us/">о нас</a>
                </li>
            </ul>
        </nav>
    </header>
    <header class="header header_right">
        <nav class="header__menu">
            <ul class="header__menu__parent">
                <li class="menu-item">
                    <a href="https://mynudenymph.com/?s=">
                        поиск
                    </a>
                </li>
                <li class="menu-item">
                    <a href="https://mynudenymph.com/cart/" class="header__menu__cart">
                        <span>корзина</span>
                        <span class="cart-counter"></span>
                    </a>
                </li>
                <li class="menu-item enter-profile">
                </li>
            </ul>
        </nav>
    </header>

    <header class="header header_mobile ">
        <input type="checkbox" id="toggle" class="header__burger__input" autocomplete="off" />
        <label class="header__burger__btn" for="toggle">
            <div class="bar"></div>
            <div class="bar"></div>
            <div class="bar"></div>
        </label>
        <a href="https://mynudenymph.com" class="header__logo">
            <svg>
                <use xlink:href='#logo' />
            </svg>
        </a>
        <a href="https://mynudenymph.com/?s=" class="header__search">
            <svg>
                <use xlink:href='#search' />
            </svg>
        </a>
        <a href="https://mynudenymph.com/cart/" class="header__cart">
            <span class="header__cart__text cart-counter"></span>
            <svg>
                <use xlink:href='#cart' />
            </svg>
        </a>
        <div class="header__burger__menu">
            <nav class="header__menu">
                <ul class="header__menu__parent">

                    <li class=" menu-item menu-item-type-custom menu-item-object-custom menu-item-34 menu-item-has-children ">
                        <a target="" href="#">магазин</a>
                        <ul class="header__menu__children">

                            <li class=" menu-item menu-item-type-post_type menu-item-object-page menu-item-173 ">
                                <a target="" href="https://mynudenymph.com/shop/">смотреть все</a>
                            </li>

                            <li class="menu-item_outlet menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-22221 ">
                                <a target="" href="https://mynudenymph.com/shop/outlet/">распродажа</a>
                            </li>

                            <li class=" menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-1222 ">
                                <a target="" href="https://mynudenymph.com/shop/beachwear/">купальники</a>
                            </li>

                            <li class=" menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-3127 ">
                                <a target="" href="https://mynudenymph.com/shop/apparel/">одежда</a>
                            </li>

                            <li class=" menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-11030 ">
                                <a target="" href="https://mynudenymph.com/shop/activewear/">для спорта</a>
                            </li>

                            <li class=" menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-2998 ">
                                <a target="" href="https://mynudenymph.com/shop/accessories/">аксессуары</a>
                            </li>

                            <li class=" menu-item menu-item-type-post_type menu-item-object-product menu-item-4809 ">
                                <a target="" href="https://mynudenymph.com/product/gift-card/">подарочные карты</a>
                            </li>
                        </ul>
                    </li>

                    <li class=" menu-item menu-item-type-custom menu-item-object-custom menu-item-3040 menu-item-has-children ">
                        <a target="" href="#">коллекции</a>
                        <ul class="header__menu__children">

                            <li class=" menu-item menu-item-type-post_type menu-item-object-page menu-item-27821 ">
                                <a target="" href="https://mynudenymph.com/collection-summer-22/">Лето ’22</a>
                            </li>

                            <li class=" menu-item menu-item-type-post_type menu-item-object-page menu-item-17944 ">
                                <a target="" href="https://mynudenymph.com/tales/">Tales</a>
                            </li>

                            <li class=" menu-item menu-item-type-post_type menu-item-object-page menu-item-4401 ">
                                <a target="" href="https://mynudenymph.com/lido/">Lido</a>
                            </li>

                            <li class=" menu-item menu-item-type-post_type menu-item-object-page menu-item-3038 ">
                                <a target="" href="https://mynudenymph.com/collection-spring-21/">Весна ’21</a>
                            </li>

                            <li class=" menu-item menu-item-type-post_type menu-item-object-page menu-item-3037 ">
                                <a target="" href="https://mynudenymph.com/collection-winter-21/">New Year ’21</a>
                            </li>

                            <li class=" menu-item menu-item-type-post_type menu-item-object-page menu-item-3036 ">
                                <a target="" href="https://mynudenymph.com/collection-summer-20/">Лето ’20</a>
                            </li>
                        </ul>
                    </li>

                    <li class=" menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor current-menu-parent menu-item-3041 menu-item-has-children active">
                        <a target="" href="#">помощь</a>
                        <ul class="header__menu__children">

                            <li class=" menu-item menu-item-type-post_type menu-item-object-page menu-item-3039 ">
                                <a target="" href="https://mynudenymph.com/size-table/">таблица размеров</a>
                            </li>

                            <li class=" menu-item menu-item-type-post_type menu-item-object-page menu-item-3061 ">
                                <a target="" href="https://mynudenymph.com/shipping-and-returns/">доставка и возврат</a>
                            </li>

                            <li class=" menu-item menu-item-type-post_type menu-item-object-page menu-item-3059 ">
                                <a target="" href="https://mynudenymph.com/discounts/">акции и скидки</a>
                            </li>

                            <li class=" menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-3053 current_page_item menu-item-3060 active">
                                <a target="" href="https://mynudenymph.com/faq/">FAQ</a>
                            </li>
                        </ul>
                    </li>

                    <li class=" menu-item menu-item-type-post_type menu-item-object-page menu-item-27962 ">
                        <a target="" href="https://mynudenymph.com/about-us/">о нас</a>
                    </li>
                </ul>
                <span class="enter-profile-mobile">

            </span>
            </nav>
            <div class="header__burger__menu__footer">
                <a class="header__settings open-modal" data-modal="#geography-zone-modal">
                    <div class="header__settings__descr">
                        <span>&#8381; / ru /</span>
                        <img src="https://mynudenymph.com/wp-content/plugins/mynudenymph-geography-zones/public/flags/de.png" alt="flag-country">
                    </div>
                    <p class="header__settings__link">change</p>
                </a>
                <div class="header__links">
                    <a href="https://mynudenymph.com/cart/" class="header__cart">
                        <span class="header__cart__text">0</span>
                        <svg>
                            <use xlink:href='#cart' />
                        </svg>
                    </a>
                    <a href="https://mynudenymph.com/my-account/" class="header__account " data-modal="#form-login-modal" data-link="https://mynudenymph.com/my-account/">
                        <svg>
                            <use xlink:href='#account' />
                        </svg>
                    </a>
                </div>
            </div>
        </div>
    </header>
